#!/usr/bin/env python

from bs4 import BeautifulSoup
import requests

def main():
    print get_formatted_data('SamseedXxl')

get_destiny_url = 'http://destinytracker.com/destiny/games/xbox/{}?mode=7'.format

def get_destiny_data(name):
    return BeautifulSoup(requests.get(get_destiny_url(name)).content)

def get_formatted_data(name):
    s = get_destiny_data(name)
    return [
        to_formatted_data(node)
        for node
        in s.find_all(class_='games-table-row')
    ]

def to_formatted_data(node):
    return {
        'title': get_title(node),
        'score': get_score(node),
        'kills': get_kills(node),
        'deaths': get_deaths(node),
        'assists': get_assists(node),
        'standing': get_standing(node)
    }

def get_title(node):
    return node.span.text.strip()

def get_score(node):
    return node.find(class_='score').text

def get_kills(node):
    return node.find(class_='kills').text

def get_deaths(node):
    return node.find(class_='deaths').text

def get_assists(node):
    return node.find(class_='assists').text

def get_standing(node):
    return node.find(class_='standing').text

if __name__ == '__main__':
    main()
